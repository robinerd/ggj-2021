﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSound : MonoBehaviour
{
    [Tooltip("Leave empty to take from audio source")]
    public AudioClip[] clips;
    public bool playOnAwake = true;
    public float pitchRandomness = 0.1f;
    public float volumeRandomness = 0.2f;
    [Tooltip("Randomize pitch between several tones to fit like a chord. Applied as an offset on the base pitch.")]
    public int[] randomSemitones;

    AudioSource source;
    float originalPitch;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        originalPitch = source.pitch;

        if (playOnAwake)
            Play();
    }

    public void Play()
    {
        float pitch = originalPitch;
        if(randomSemitones.Length > 0)
        {
            var semitone = randomSemitones[Random.Range(0, randomSemitones.Length)];
            pitch *= Mathf.Pow(2, semitone/12.0f);
        }

        source.pitch = pitch * Random.Range(1 - pitchRandomness, 1 + pitchRandomness);
        var clip = clips.Length > 0 ? clips[Random.Range(0, clips.Length)] : source.clip;
        source.PlayOneShot(clip, source.volume * Random.Range(1 - volumeRandomness, 1 + volumeRandomness));
    }
}
