﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class ActivateWithFishes : MonoBehaviour
{
    public bool requireAllFishes = true;
    public int fishesRequired = 0;
    public float activationRange = 1;
    public GameObject nearbyFeedback;
    public TMPro.TextMeshPro fishCounterText;

    public UnityEvent OnActivate = new UnityEvent();
    public UnityEvent OnDeactivate = new UnityEvent();

    bool activated = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    void FixedUpdate()
    {
        Player player = BoidManager.instance.player?.GetComponent<Player>();
        if (player == null)
            return;

        if (requireAllFishes && fishesRequired == 0)
        {
            fishesRequired = player.allFish.Length;
        }

        var fishesCurrent = FishCountWithTarget(player.allFish, player.transform);

        if (!activated)
        {

            if (Vector2.Distance(player.transform.position, transform.position) <= activationRange)
            {
                if (fishesCurrent >= fishesRequired)
                {
                    Activate(player);
                }
            }
        }
        else
        {
            fishesCurrent = FishCountWithTarget(player.allFish, transform); //Count fishes already following instead.
            if (fishesCurrent < fishesRequired)
            {
                Deactivate();
            }
        }

        if (fishCounterText)
            fishCounterText.text = fishesCurrent + "/" + fishesRequired;
    }

    public static int FishCountWithTarget(Fish[] allFish, Transform targetRequired)
    {
        return allFish.Count(fish => fish.GetComponent<Boid>().target == targetRequired);
    }

    public void Activate(Player player)
    {
        if (activated)
            return;

        // Make the required fishes stay by the gate until player pings them again.
        int fishesPaid = 0;
        foreach(var fish in player.allFish)
        {
            if(fish.GetComponent<Boid>().target == player.transform)
            {
                fish.GetComponent<Boid>().target = transform; // Change target to this gate, stop following player.
                fishesPaid++;
                if(fishesPaid >= fishesRequired)
                {
                    break;
                }
            }
        }

        activated = true;
        OnActivate?.Invoke();
    }

    public void Deactivate()
    {
        if (!activated)
            return;

        activated = false;
        OnDeactivate?.Invoke();
    }
}
