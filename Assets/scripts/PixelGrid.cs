﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public struct PixelCell
{
    public bool solid;
    public float position;
    public float velocity;
    public Color originalColor;
}

public class PixelGrid : MonoBehaviour
{
    public static PixelGrid instance;

    [FormerlySerializedAs("groundMask")]
    public Texture2D levelTexture;

    public Transform mainLightSource;
    public float lightRange = 20;
    public int lightSteps = 16;
    public float lightFromWaves = 1;
    public Gradient colors;
    public bool simulate = true;
    public float gridDeltaTime = 0.1f;
    public float springForce = 1;
    public float springDamping = 0.2f;
    public float globalDamping = 0.995f;
    public float dampMultiplierUp = 1;
    public float dampMultiplierDown = 1;
    public float springMultiplierUp = 1;
    public float springMultiplierDown = 1;

    public float noise = 0.2f;
    public int width = 10;
    public int height = 10;

    [Header("Debugging")]
    public bool useMouseClickDebug = false;

    PixelCell[,] grid;
    PixelCell[,] nextGrid;
    Texture2D texture;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<MeshRenderer>().material.mainTexture)
        {
            texture = Instantiate(GetComponent<MeshRenderer>().material.mainTexture) as Texture2D;
        }
        else
        {
            texture = new Texture2D(width, height);
            texture.filterMode = FilterMode.Point;
        }

        width = texture.width;
        height = texture.height;

        grid = new PixelCell[width, height];
        nextGrid = new PixelCell[width, height];

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (levelTexture)
                {
                    grid[x, y].solid = levelTexture.GetPixel(x, y).a > 0.5f;
                    nextGrid[x, y].solid = grid[x, y].solid;

                    grid[x, y].originalColor = grid[x, y].solid ? levelTexture.GetPixel(x, y) : texture.GetPixel(x, y);
                    nextGrid[x, y].originalColor = grid[x, y].originalColor;
                }
                else
                {
                    // Empty level: background color only, and water on top1
                    grid[x, y].solid = false;
                    nextGrid[x, y].solid = false;
                    grid[x, y].originalColor = texture.GetPixel(x, y);
                    nextGrid[x, y].originalColor = grid[x, y].originalColor;
                }
            }
        }

        GetComponent<MeshRenderer>().material.mainTexture = texture;

        StartCoroutine(UpdateGridLoop());
    }

    public void EmitLong(Vector3 position, float magnitude, float duration)
    {
        StartCoroutine(EmitLong_Coroutine(position, magnitude, duration));
    }

    IEnumerator EmitLong_Coroutine(Vector3 position, float magnitude, float duration)
    {
        while(duration >= 0 && gameObject != null && isActiveAndEnabled)
        {
            Emit(position, magnitude);
            yield return new WaitForSeconds(gridDeltaTime);
            duration -= gridDeltaTime;
        }
    }

    Vector2Int WorldToGridPos(Vector2 worldPos)
    {
        var posLocal = transform.InverseTransformPoint(worldPos);
        int gridX = Mathf.RoundToInt(width * (posLocal.x + 0.5f));
        int gridY = Mathf.RoundToInt(height * (posLocal.y + 0.5f));
        return new Vector2Int(gridX, gridY);
    }

    public void Emit(Vector3 position, float magnitude)
    {
        var gridCoord = WorldToGridPos(position);
        int x = gridCoord.x;
        int y = gridCoord.y;

        if (x >= 0 && x < width && y >= 0 && y< height && !grid[x, y].solid)
        {
            grid[x, y].position = magnitude;
        }
    }

    IEnumerator UpdateGridLoop()
    {
        while (gameObject)
        {
            if (isActiveAndEnabled)
            {
                UpdateGrid();
                yield return new WaitForSeconds(gridDeltaTime);
            }
        }
    }

    [ContextMenu("Update Grid One Frame")]
    public void UpdateGrid()
    {
        if (useMouseClickDebug && Input.GetMouseButton(0))
        {
            var mouseWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Emit(mouseWorld, 3);
        }

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (grid[x, y].solid)
                    continue;

                float springR = (x + 1 <= width - 1 ? grid[x + 1, y].position : 0) - grid[x, y].position;
                float springL = (x - 1 >= 0 ? grid[x - 1, y].position : 0) - grid[x, y].position;
                float springU = (y + 1 <= height - 1 ? grid[x, y + 1].position : 0) - grid[x, y].position;
                float springD = (y - 1 >= 0 ? grid[x, y - 1].position : 0) - grid[x, y].position;

                float dampR = (x + 1 <= width - 1 ? grid[x + 1, y].velocity : 0) - grid[x, y].velocity;
                float dampL = (x - 1 >= 0 ? grid[x - 1, y].velocity : 0) - grid[x, y].velocity;
                float dampU = (y + 1 <= height - 1 ? grid[x, y + 1].velocity : 0) - grid[x, y].velocity;
                float dampD = (y - 1 >= 0 ? grid[x, y - 1].velocity : 0) - grid[x, y].velocity;

                nextGrid[x, y].velocity = grid[x, y].velocity + (springR + springL + springU * springMultiplierUp + springD * springMultiplierDown) * springForce * gridDeltaTime;
                nextGrid[x, y].velocity += (dampR + dampL + dampU * dampMultiplierUp + dampD * dampMultiplierDown) * springDamping * gridDeltaTime;
                nextGrid[x, y].velocity *= globalDamping;
                nextGrid[x, y].position = grid[x, y].position + nextGrid[x, y].velocity * gridDeltaTime;
                nextGrid[x, y].position += Random.Range(-1f, 1f) * noise;
            }
        }

        var oldGrid = grid;
        grid = nextGrid;
        nextGrid = oldGrid;

        RenderGrid();
    }

    Color[] pixelColors = null; //Reused
    float[] lightMask;
    Color[] pixelsColorsLight = null; //Reused

    [ContextMenu("Render Grid")]
    void RenderGrid()
    {
        if (pixelColors == null || pixelColors.Length != width * height)
        {
            pixelColors = texture.GetPixels();
        }
        if (lightMask == null || lightMask.Length != width * height)
        {
            lightMask = new float[width * height];
        }
        if (pixelsColorsLight == null || pixelsColorsLight.Length != width * height)
        {
            pixelsColorsLight = new Color[width * height];
        }

        Vector2 mainLightSourceGridPos = Vector2Int.zero;
        if (mainLightSource)
            mainLightSourceGridPos = WorldToGridPos(mainLightSource.position);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int i = y * width + x;

                pixelColors[i] = grid[x, y].originalColor;

                if (!grid[x, y].solid)
                {
                    pixelColors[i] += colors.Evaluate(Mathf.Clamp01(grid[x, y].position * 0.5f + 0.5f));
                }

                float lightness = 1 - Mathf.Clamp01(Vector2.Distance(mainLightSourceGridPos, new Vector2(x, y)) / lightRange);
                lightness += movementInPoint(x, y) * lightFromWaves;

                lightMask[i] = lightness * 0.2f + lightMask[i] * 0.8f;
            }
        }

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int i = y * width + x;
                pixelsColorsLight[i] = pixelColors[i] * LightnessInPoint(x,y);
            }
        }
        texture.SetPixels(pixelsColorsLight);
        texture.Apply(false);
    }

    float movementInPoint(int x, int y)
    {
        if (x < 0 || x >= width || y < 0 || y >= height)
            return 0;

        return /*Mathf.Clamp01*/(grid[x, y].velocity * grid[x, y].velocity + grid[x, y].position * grid[x, y].position);
    }


    public float LightnessInPointWorld(Vector2 worldPosition)
    {
        var gridPos = WorldToGridPos(worldPosition);
        return LightnessInPoint(gridPos.x, gridPos.y);
    }

    public float LightnessInPoint(int midX, int midY)
    {
        float sum = 0;
        int radius = 1;

        for (int y = midY - radius; y <= midY + radius; y++)
        {
            if (y < 0 || y >= height)
                continue;

            for (int x = midX - radius; x <= midX + radius; x++)
            {
                if (x < 0 || x >= width)
                    continue;

                sum += lightMask[y * width + x];
            }
        }

        float lightness = sum / ((radius*2+1) * (radius*2+1));

        //lightness = Mathf.Pow(Mathf.Clamp01(lightness), 0.7f);
        lightness = Mathf.Clamp01(lightness);
        lightness = ((int)(lightness * lightSteps + 0.5f)) / (float)lightSteps;

        return lightness;
    }
}
