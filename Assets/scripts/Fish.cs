﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Fish : MonoBehaviour
{
    public bool followsPlayer { get; private set; }
    public UnityEvent OnPingReceived = new UnityEvent();
    public UnityEvent OnStartFollow = new UnityEvent();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ReceivePing(Player player)
    {
        OnPingReceived?.Invoke();

        if (GetComponent<Boid>().target != player.transform)
        {
            GetComponent<Boid>().target = player.transform;
            OnStartFollow?.Invoke();
        }
    }

    public void ReceivePingDelayed(Player player, float delay)
    {
        StartCoroutine(ReceivePingDelayed_Coroutine(player, delay));
    }

    private IEnumerator ReceivePingDelayed_Coroutine(Player player, float delay)
    {
        yield return new WaitForSeconds(delay);

        if (isActiveAndEnabled)
        {
            ReceivePing(player);
        }
    }
}
