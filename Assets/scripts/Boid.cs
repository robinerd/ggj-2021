using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour
{
    Rigidbody2D body;
    //float course = 1;
    public float speed = 0.5f;
    private float angle = 0.01f;
    //public Vector2 velocity;
    //public float force;
    float originalDistance = 1;
    public float distanceForGroup = 2f;
    public float distanceForNeighbour = 0.5f;
    public float moveToTargetExtra = 1f;
    public float followPlayerDistance = 1f;
    List<Boid> myGroup = new List<Boid>();
    public bool NoBoidMovement = false;

    public Transform target;
    private bool isColliding = false;
    private Vector2 normalOfCollision;
    private Vector2 startPosition;
    //private bool found;

    bool dontFollowPlayer => target == null || target != BoidManager.instance.player.transform;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        if (!NoBoidMovement)
        {
            if (body == null)
                gameObject.AddComponent<Rigidbody2D>();
            body.gravityScale = 0;
        }
        //force = Mathf.Sqrt(5 / transform.position.magnitude);
        originalDistance = Vector3.Distance(transform.position, Vector3.zero);
        //originalDistance = Random.Range(8, 16);
        //Debug.Log("my distance: " + originalDistance);
        startPosition = transform.position;
        if (BoidManager.instance == null)
        {
            GameObject empty = Instantiate(new GameObject(), null);
            empty.name = "BOID MANAGER";
            empty.AddComponent<BoidManager>();
        }
        distanceForNeighbour *= Random.Range(0.9f, 1.1f);
        BoidManager.instance.allBoids.Add(this);

        //speed *= Random.Range(0.2f, 1.6f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!NoBoidMovement)
        {
            //UpdateMyGroup();
            var v1 = Separation()*1.4f;
            var v2 = Cohesion();
            var v3 = Alignment();

            var v4 = target != null ? CircleAroundPoint(target.position) * 0.2f : CircleAroundPoint(startPosition) * 0.2f;
            var v6 = target == null ? OnCollided()*0.7f : OnCollided() * 0.3f;
            //if(Input.GetMouseButtonDown(0))
            //var v5 = MoveTowardsPoint(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            var v5 = target != null ? MoveTowardsPoint(target.position) * Mathf.InverseLerp(0.5f, 6,Vector2.Distance(transform.position, target.position))*3.5f : MoveTowardsPoint(startPosition + new Vector2(Mathf.Cos(Time.time),Mathf.Sin(Time.time * 1.3f))) * 0.7f;

            //Maybe we should have a fun path thing that is randomized?
            //var v5 = FollowPlayer();
            Move(v1 + v2 + v3 + v5 + v4 + v6);
        }
     }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        normalOfCollision = collision.contacts[0].normal;
        isColliding = true;
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        //normalOfCollision = Vector2.zero;
        //isColliding = false;
    }
    public Vector2 OnCollided()
    {
        //Yea get the fuck out of here
        if(isColliding)
        {

            return normalOfCollision.normalized;
        }
        return Vector2.zero;
    }
    public Vector2 FollowPlayer()
    {
        var velocity = Vector2.zero;
        if (BoidManager.instance.player != null && 
            followPlayerDistance > Vector2.Distance(BoidManager.instance.player.transform.position, transform.position) &&
            followPlayerDistance/10 < Vector2.Distance(BoidManager.instance.player.transform.position, transform.position))
        {
            velocity = MoveTowardsPoint(BoidManager.instance.player.transform.position);
            //if(!found)
            //{
            //    found = true;
            //    GetComponent<WaveEmitter>().Emit(5);
            //}
        }
        else
        {
            //found = false;
        }
            
        return velocity;
    }


    public void UpdateMyGroup()
    {
        foreach(var b in BoidManager.instance.allBoids)
        {
           if(myGroup.Contains(b))
            {
                if (distanceForGroup < Vector3.Distance(transform.position, b.transform.position))
                {
                    myGroup.Remove(b);

                    if (b.transform == target)
                    {
                        // Stopped following the target. Hold ground and idle!
                        target = null;
                        startPosition = transform.position;
                    }
                }
            }
           else
            {
                if (distanceForGroup > Vector3.Distance(transform.position, b.transform.position))
                {
                    //part of group
                    myGroup.Add(b);
                }
            }
        }
    }
    public Vector2 CircleAroundPoint(Vector3 point)
    {
        //Vector3 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        point = new Vector3(point.x, point.y, 0);

        Vector2 centripetalForceDirection = (point - transform.position).normalized;
        Vector2 tangentVelocityDirection = GetPerPendicularDirection(centripetalForceDirection);
        var velocity =  tangentVelocityDirection;
        return velocity;
    }

    public Vector2 MoveTowardsPoint(Vector3 point)
    {
        //if(Vector3.Distance(point,transform.position)> originalDistance)
        //{
        point = new Vector3(point.x, point.y, 0);
        Vector2 centripetalForceDirection = (point - transform.position).normalized;

        var velocity = centripetalForceDirection * moveToTargetExtra;
        return velocity.normalized;
        //}
        //else
        //{
        //    return Vector2.zero;
        //}
    }
    public Vector2 Separation()
    {
        //We should keep a min distance from each other
        Vector2 velocity = Vector2.zero;
        Vector2 averageVector = Vector3.zero;
        foreach (var b in myGroup)
        {
            if (dontFollowPlayer && b.transform == BoidManager.instance.player.transform)
                continue;

            if (distanceForNeighbour > Vector3.Distance(transform.position, b.transform.position))
            {
                Vector2 towardsMe = transform.position - b.transform.position;

                if (towardsMe.magnitude > 0)
                {
                    averageVector += towardsMe.normalized / towardsMe.magnitude;
                }
            }

        }

        if (averageVector != Vector2.zero)
        {
            velocity = averageVector.normalized;
        }
        return velocity;
    }

    public Vector2 Cohesion()
    {
        //We should move to our center of mass
        Vector2 velocity = Vector2.zero;
        Vector3 averagePosition = Vector3.zero;
        int count = 0;
        foreach (var b in myGroup)
        {
            if (dontFollowPlayer && b.transform == BoidManager.instance.player.transform)
                continue;
            averagePosition += b.transform.position;
            count++;
        }
        averagePosition /= count;

        if (averagePosition != Vector3.zero)
        {
            velocity = (averagePosition - transform.position).normalized;
        }
        return velocity;
    }
    public Vector2 Alignment()
    {
        //We should try to keep the same direction
        var velocity = Vector2.zero;
        var averageVelocity = Vector2.zero;
        foreach (var b in myGroup)
        {
            if (dontFollowPlayer && b.transform == BoidManager.instance.player.transform)
                continue;

            averageVelocity += b.body.velocity;
        }
        if (averageVelocity != Vector2.zero)
        {
            velocity = averageVelocity.normalized;
        }
        return velocity;
    }

    public void Move(Vector2 newVelocity)
    {
        if(target == null)
        {
            body.velocity = newVelocity * speed *0.5f;
        }
        else
        {
            body.velocity = newVelocity * speed;
        }
        

        angle = Mathf.Atan2(newVelocity.y, newVelocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
    private Vector2 GetPerPendicularDirection(Vector2 v)
    {
        return (new Vector2(v.y, -v.x)).normalized;
    }
}
