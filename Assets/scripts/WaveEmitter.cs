﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveEmitter : MonoBehaviour
{
    public float pulseMagnitude = 2;
    public float pulseDuration = 0;
    public float wavesPerSecond = 0;
    public float onCollideMagnitude = 0;
    public float randomize = 0;

    float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        if(randomize > 0)
        {
            pulseMagnitude *= 1 + Random.Range(0, randomize);
            wavesPerSecond *= 1 + Random.Range(0, randomize);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(wavesPerSecond > 0)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                timer = 1.0f / wavesPerSecond;
                Emit();
            }
        }
    }

    public void Emit()
    {
        Emit(1.0f);
    }

    public void Emit(float magnitudeMultiplier)
    {
        if (pulseDuration > 0)
            PixelGrid.instance.EmitLong(transform.position, pulseMagnitude * magnitudeMultiplier, pulseDuration);
        else
            PixelGrid.instance.Emit(transform.position, pulseMagnitude * magnitudeMultiplier);
    }
}
