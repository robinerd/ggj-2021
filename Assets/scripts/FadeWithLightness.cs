﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FadeWithLightness : MonoBehaviour
{
    SpriteRenderer[] sprites;
    Color[] originalSpriteColors;
    TextMeshPro[] texts;
    Color[] originalTextColors;
    TrailRenderer[] trails;
    Gradient[] originalTrailGradients;
    float lightness = 0;

    // Start is called before the first frame update
    void Start()
    {
        sprites = GetComponentsInChildren<SpriteRenderer>(true);
        originalSpriteColors = new Color[sprites.Length];
        for (int i = 0; i < sprites.Length; i++)
        {
            originalSpriteColors[i] = sprites[i].color;
            sprites[i].color = new Color(0, 0, 0, 0);
        }

        trails = GetComponentsInChildren<TrailRenderer>(true);
        originalTrailGradients = new Gradient[trails.Length];
        for (int i = 0; i < trails.Length; i++)
        {
            originalTrailGradients[i] = trails[i].colorGradient;
            SetTrailAlpha(i, 0);
        }

        //Don't find in children for now, goal canvas has some texts we don't want to affect
        texts = GetComponents<TextMeshPro>();
        originalTextColors = new Color[texts.Length];
        for (int i = 0; i < texts.Length; i++)
        {
            originalTextColors[i] = texts[i].color;
            texts[i].color = new Color(0, 0, 0, 0);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        lightness = Mathf.Lerp(lightness, PixelGrid.instance.LightnessInPointWorld(transform.position), 0.1f);

        for (int i = 0; i < sprites.Length; i++)
        {
            var newColor = originalSpriteColors[i];
            newColor.a *= lightness;
            sprites[i].color = newColor;
        }

        for (int i = 0; i < texts.Length; i++)
        {
            var newColor = originalTextColors[i];
            newColor.a *= lightness;
            texts[i].color = newColor;
        }

        for (int i = 0; i < trails.Length; i++)
        {
            SetTrailAlpha(i, lightness);
        }
    }

    void SetTrailAlpha(int lineIndex, float alpha)
    {
        var newKeys = new List<GradientAlphaKey>(trails[lineIndex].colorGradient.alphaKeys).ToArray();
        for (int i = 0; i < newKeys.Length; i++)
        {
            var key = newKeys[i];
            key.alpha = originalTrailGradients[lineIndex].alphaKeys[i].alpha * alpha;
            newKeys[i] = key;
        }

        Gradient newGrad = trails[lineIndex].colorGradient;
        newGrad.mode = originalTrailGradients[lineIndex].mode;
        newGrad.SetKeys(trails[lineIndex].colorGradient.colorKeys, newKeys);
        trails[lineIndex].colorGradient = newGrad;
    }
}
