using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidManager : MonoBehaviour
{
    public static BoidManager instance;
    public List<Boid> allBoids = new List<Boid>();
    public GameObject player;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //lets calculate groups with an eurmator :D
        StartCoroutine(FetchOnePerFrame());
    }
    IEnumerator FetchOnePerFrame()
    {
        int index = 0;
        while(true)
        {
            allBoids[index].UpdateMyGroup();
            index++;
            if (index == allBoids.Count)
                index = 0;
            yield return null;
        }
    }
}
