﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnAwake : MonoBehaviour
{
    public UnityEvent OnAwakeCallback = new UnityEvent();

    void Awake()
    {
        OnAwakeCallback?.Invoke();
    }
}
