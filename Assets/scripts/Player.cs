﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    public float moveForce = 100;
    public float pingCooldown = 1;
    public float pingMaxRange = 2;
    public float pingDelayFactor = 1f;
    public float pingWaveMagnitude = 2;
    public float pingWaveDuration = 0.1f;
    public AudioSource[] musics;
    public UnityEvent OnPing = new UnityEvent();

    float lastPingTime = 0;
    public Fish[] allFish { get; private set; }

    public List<Fish> followers = new List<Fish>();

    // Start is called before the first frame update
    void Start()
    {
        if(BoidManager.instance != null)
        {
            BoidManager.instance.player = gameObject;
        }
            
        allFish = FindObjectsOfType<Fish>();
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.Space))
            Ping();

        int followerCount = ActivateWithFishes.FishCountWithTarget(allFish, transform);
        float followerRatio = followerCount / (float)allFish.Length;
        //musics[0].volume = 1;
        musics[1].volume = Mathf.Lerp(musics[1].volume, followerRatio > 0.01f ? 1 : 0, 0.07f);
        musics[2].volume = Mathf.Lerp(musics[2].volume, followerRatio > 0.5f ? 1 : 0, 0.07f);
        musics[3].volume = Mathf.Lerp(musics[3].volume, followerRatio > 0.9f ? 1 : 0, 0.07f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 direction = Vector2.right * Input.GetAxis("Horizontal") + Vector2.up * Input.GetAxis("Vertical");
        if (Input.GetMouseButton(0))
        {
            direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        }

        if (direction.magnitude > 1)
            direction.Normalize();

        Move(direction);
    }

    void Ping()
    {
        if (Time.time - lastPingTime > pingCooldown)
        {
            lastPingTime = Time.time;
            PixelGrid.instance.EmitLong(transform.position, pingWaveMagnitude, pingWaveDuration);

            foreach(var fish in allFish)
            {
                float distance = Vector2.Distance(transform.position, fish.transform.position);
                float delay = distance * pingDelayFactor + 0.2f;

                //Fish will move during delay but wave origin will not. Compensate the timing!
                Vector2 fishPositionSoon = (Vector2)fish.transform.position + delay * fish.GetComponent<Rigidbody2D>().velocity;
                float distanceSoon = Vector2.Distance(transform.position, fishPositionSoon);
                float averageDistance = (distanceSoon + distance) * 0.5f; //The truth is somewhere in between
                if (averageDistance > pingMaxRange)
                    continue;

                float delayCompensated = averageDistance * pingDelayFactor;

                fish.ReceivePingDelayed(this, delayCompensated);
            }

            OnPing?.Invoke();
        }
    }

    void Move(Vector2 direction)
    {
        GetComponent<Rigidbody2D>().AddForce(direction * moveForce);
    }
}
