﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalGate : MonoBehaviour
{
    public Canvas winCanvas;
    bool won = false;
    float waitTimer = 3;

    public void Win()
    {
        if (won)
            return;

        won = true;
        StartCoroutine(WinEffect());
        StartCoroutine(FadeInWinCanvas());
    }

    void Update()
    {
        if (won)
        {
            waitTimer -= Time.deltaTime;
            if (waitTimer <= 0 && Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings);
            }
        }
    }

    IEnumerator WinEffect()
    {
        PixelGrid.instance.EmitLong(transform.position, 3, 10000);

        float dt = 0.05f;
        for(float t = 0; t < 8; t += dt)
        {
            PixelGrid.instance.lightRange += 30 * dt;
            yield return new WaitForSeconds(dt);
        }
    }

    IEnumerator FadeInWinCanvas()
    {
        if (winCanvas)
        {
            var renderers = winCanvas.GetComponentsInChildren<CanvasRenderer>();

            float dt = 0.03f;
            float duration = 3;
            for (float t = 0; t < duration; t += dt)
            {
                foreach (var rend in renderers)
                {
                    rend.SetAlpha(t / duration);
                }
                yield return new WaitForSeconds(dt);
            }
        }
    }
}
